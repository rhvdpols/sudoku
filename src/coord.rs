use super::set::Set;

#[derive(PartialEq, Debug)]
pub struct Coord {
    row: u32,
    col: u32,
}

impl Coord {
    pub fn to_parts_for_set(&self, set: &Set) -> (u32, u32) {
        match set {
            Set::Rows => (self.row, self.col),
            Set::Columns => (self.col, self.row),
            Set::Blocks => {
                let block = (self.row / 3) * 3 + self.col / 3;
                let index = (self.row % 3) * 3 + self.col % 3;

                (block, index)
            }
        }
    }

    pub fn from_parts_for_set(first: u32, second: u32, set: &Set) -> Coord {
        match set {
            Set::Rows => Coord {
                row: first,
                col: second,
            },
            Set::Columns => Coord {
                row: second,
                col: first,
            },
            Set::Blocks => {
                let row = (first / 3) * 3 + second / 3;
                let col = (first % 3) * 3 + second % 3;

                Coord { row, col }
            }
        }
    }

    pub fn to_vector_index(&self) -> usize {
        (self.col + self.row * 9) as usize
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn coord_convert_should_pass() {
        assert_eq!(
            (0, 4),
            Coord::from_parts_for_set(1, 1, &Set::Rows).to_parts_for_set(&Set::Blocks)
        );
        assert_eq!(
            (1, 0),
            Coord::from_parts_for_set(0, 3, &Set::Rows).to_parts_for_set(&Set::Blocks)
        );
        assert_eq!(
            (2, 8),
            Coord::from_parts_for_set(2, 8, &Set::Rows).to_parts_for_set(&Set::Blocks)
        );

        assert_eq!(
            (3, 0),
            Coord::from_parts_for_set(0, 3, &Set::Columns).to_parts_for_set(&Set::Blocks)
        );
        assert_eq!(
            (4, 5),
            Coord::from_parts_for_set(5, 4, &Set::Columns).to_parts_for_set(&Set::Blocks)
        );
        assert_eq!(
            (5, 6),
            Coord::from_parts_for_set(6, 5, &Set::Columns).to_parts_for_set(&Set::Blocks)
        );

        assert_eq!(
            (4, 1),
            Coord::from_parts_for_set(3, 4, &Set::Blocks).to_parts_for_set(&Set::Rows)
        );
        assert_eq!(
            (5, 4),
            Coord::from_parts_for_set(4, 7, &Set::Blocks).to_parts_for_set(&Set::Rows)
        );
        assert_eq!(
            (3, 7),
            Coord::from_parts_for_set(5, 1, &Set::Blocks).to_parts_for_set(&Set::Rows)
        );

        assert_eq!(
            (2, 6),
            Coord::from_parts_for_set(6, 2, &Set::Blocks).to_parts_for_set(&Set::Columns)
        );
        assert_eq!(
            (3, 6),
            Coord::from_parts_for_set(7, 0, &Set::Blocks).to_parts_for_set(&Set::Columns)
        );
        assert_eq!(
            (6, 7),
            Coord::from_parts_for_set(8, 3, &Set::Blocks).to_parts_for_set(&Set::Columns)
        );
    }

    #[test]
    fn coord_to_vector_index() {
        assert_eq!(
            0,
            Coord::from_parts_for_set(0, 0, &Set::Rows).to_vector_index()
        );
        assert_eq!(
            8,
            Coord::from_parts_for_set(0, 8, &Set::Rows).to_vector_index()
        );
        assert_eq!(
            9,
            Coord::from_parts_for_set(0, 1, &Set::Columns).to_vector_index()
        );
        assert_eq!(
            80,
            Coord::from_parts_for_set(8, 8, &Set::Blocks).to_vector_index()
        );
    }
}
