use std::collections::HashSet;
use std::collections::VecDeque;
use std::fmt;

use super::coord::Coord;
use super::field::Field;
use super::set::Set;
use super::smove::Move;

#[derive(Clone)]
pub struct Possibles {
    pub rows: Vec<HashSet<u32>>,
    pub columns: Vec<HashSet<u32>>,
    pub blocks: Vec<HashSet<u32>>,
}

impl Possibles {
    fn new() -> Possibles {
        let mut rows = Vec::new();
        let mut columns = Vec::new();
        let mut blocks = Vec::new();

        for _ in 0..9 {
            rows.push((0..9).collect());
            columns.push((0..9).collect());
            blocks.push((0..9).collect());
        }

        Possibles {
            rows,
            columns,
            blocks,
        }
    }

    fn get_set(&self, set: &Set) -> &Vec<HashSet<u32>> {
        match set {
            Set::Rows => &self.rows,
            Set::Columns => &self.columns,
            Set::Blocks => &self.blocks,
        }
    }

    fn get_set_mut(&mut self, set: &Set) -> &mut Vec<HashSet<u32>> {
        match set {
            Set::Rows => &mut self.rows,
            Set::Columns => &mut self.columns,
            Set::Blocks => &mut self.blocks,
        }
    }
}

#[derive(Clone, Default)]
pub struct Data {
    pub solution: Vec<Field>,
    pub options: Vec<Vec<HashSet<u32>>>,
    pub digits: Vec<Possibles>,

    // keep track of moves done (row, col) to reject duplicate moves
    move_history: HashSet<(u32, u32)>,
}

impl Data {
    pub fn new() -> Data {
        // create fields data
        let mut solution = Vec::new();
        let mut options = Vec::new();

        for i in 0..9 {
            options.push(Vec::new());

            for j in 0..9 {
                solution.push(Field::new(i, j));

                options[i as usize].push((0..9).collect())
            }
        }

        // create digit data
        let mut digits: Vec<Possibles> = Vec::new();

        for _ in 0..9 {
            digits.push(Possibles::new());
        }

        // return new data object
        Data {
            solution,
            options,
            digits,
            move_history: HashSet::new(),
        }
    }

    pub fn set(&mut self, mv: Move, moves: &mut VecDeque<Move>) {
        self.move_history.insert((mv.row, mv.col));

        self.solution[Coord::from_parts_for_set(mv.row, mv.col, &Set::Rows).to_vector_index()]
            .set(mv.digit);

        // block coords
        let (block, index) =
            Coord::from_parts_for_set(mv.row, mv.col, &Set::Rows).to_parts_for_set(&Set::Blocks);

        // === SETS ===
        let set_iter = [Set::Rows, Set::Columns, Set::Blocks].iter();

        for set in set_iter {
            let (first, second) =
                Coord::from_parts_for_set(mv.row, mv.col, &Set::Rows).to_parts_for_set(set);

            if self.digits[mv.digit as usize].get_set(set)[first as usize].contains(&second) {
                self.digits[mv.digit as usize].get_set_mut(set)[first as usize].clear();
                self.digits[mv.digit as usize].get_set_mut(set)[first as usize].insert(second);
            } else {
                panic!(
                    "Illegal move in {:?} for digit {} at {},{}",
                    set, mv.digit, mv.row, mv.col
                );
            }
        }

        // === OPTIONS ===
        let option_row_iter = (0..9)
            .filter(|a| *a != mv.row)
            .map(|a| (mv.digit, a, mv.col));

        let option_column_iter = (0..9)
            .filter(|a| *a != mv.col)
            .map(|a| (mv.digit, mv.row, a));

        let option_block_iter = (0..9).filter(|a| *a != index).map(|a| {
            let (r, c) =
                Coord::from_parts_for_set(block, a, &Set::Blocks).to_parts_for_set(&Set::Rows);
            (mv.digit, r, c)
        });

        for (d, r, c) in option_row_iter
            .chain(option_column_iter)
            .chain(option_block_iter)
        {
            if self.options[r as usize][c as usize].remove(&d) {
                if self.options[r as usize][c as usize].len() == 1 {
                    for survivor in self.options[r as usize][c as usize].iter() {
                        push_move(
                            moves,
                            Coord::from_parts_for_set(r, c, &Set::Rows),
                            *survivor,
                            &mut self.move_history,
                        );
                    }
                } else if self.options[r as usize][c as usize].is_empty() {
                    panic!("Illegal move in options! {:?}", mv);
                }
            }
        }

        // === ROWS ===
        //
        // all rows but the current
        let row_iter = (0..9)
            .filter(|a| *a != mv.row)
            .map(|a| (mv.digit, a, mv.col, Set::Rows));

        // all digits but the current
        let row_digit_iter = (0..9)
            .filter(|a| *a != mv.digit)
            .map(|a| (a, mv.row, mv.col, Set::Rows));

        // === COLUMNS ===
        //
        // all columns but the current
        let column_iter = (0..9)
            .filter(|i| *i != mv.col)
            .map(|a| (mv.digit, mv.row, a, Set::Columns));

        // all digits but the current
        let column_digit_iter = (0..9)
            .filter(|a| *a != mv.digit)
            .map(|a| (a, mv.row, mv.col, Set::Columns));

        // === BLOCKS ===
        //
        // for all digits but the current as block index
        let block_digit_iter = (0..9)
            .filter(|a| *a != mv.digit)
            .map(|a| (a, mv.row, mv.col, Set::Blocks));

        // for all rows but the current as block index
        let block_row_iter = (0..9)
            .filter(|a| *a != mv.row)
            .map(|a| (mv.digit, a, mv.col, Set::Blocks));

        // for all columns but the current as block index
        let block_column_iter = (0..9)
            .filter(|a| *a != mv.col)
            .map(|a| (mv.digit, mv.row, a, Set::Blocks));

        // === Reverse block logic ===
        let row_block_iter = (0..9).filter(|a| *a != index).map(|a| {
            let (r, c) =
                Coord::from_parts_for_set(block, a, &Set::Blocks).to_parts_for_set(&Set::Rows);
            (mv.digit, r, c, Set::Rows)
        });

        let column_block_iter = (0..9).filter(|a| *a != index).map(|a| {
            let (r, c) =
                Coord::from_parts_for_set(block, a, &Set::Blocks).to_parts_for_set(&Set::Rows);
            (mv.digit, r, c, Set::Columns)
        });

        // === ALL TOGETHER NOW ===
        //
        // Iterate over monstrous chain
        for (d, r, c, set) in block_digit_iter
            .chain(block_column_iter)
            .chain(block_row_iter)
            .chain(column_iter)
            .chain(column_digit_iter)
            .chain(row_iter)
            .chain(row_digit_iter)
            .chain(row_block_iter)
            .chain(column_block_iter)
        {
            let (first, second) =
                Coord::from_parts_for_set(r, c, &Set::Rows).to_parts_for_set(&set);

            if self.digits[d as usize].get_set_mut(&set)[first as usize].remove(&second) {
                if self.digits[d as usize].get_set(&set)[first as usize].len() == 1 {
                    for survivor in self.digits[d as usize].get_set(&set)[first as usize].iter() {
                        push_move(
                            moves,
                            Coord::from_parts_for_set(first, *survivor, &set),
                            d,
                            &mut self.move_history,
                        );
                    }
                } else if self.digits[d as usize].get_set(&set)[first as usize].is_empty() {
                    panic!(
                        "Illlegal move {:?} in {:?} for digit {} at {},{}",
                        mv, set, d, r, c
                    );
                }
            }
        }
    }

    pub fn search(&self) -> Vec<Move> {
        let mut moves = Vec::new();

        for field in self.solution.iter() {
            if field.get().is_none() {
                // return all possible moves for this field
                let (row, col) = field.get_rowcol();

                for digit in self.options[row as usize][col as usize].iter() {
                    moves.push(Move {
                        row,
                        col,
                        digit: *digit,
                    });
                }

                return moves;
            }
        }

        moves
    }
}

fn push_move(moves: &mut VecDeque<Move>, coord: Coord, digit: u32, hist: &mut HashSet<(u32, u32)>) {
    let (row, col) = coord.to_parts_for_set(&Set::Rows);

    if !hist.contains(&(row, col)) {
        hist.insert((row, col));

        let mv = Move { row, col, digit };

        moves.push_back(mv);
    }
}

impl fmt::Display for Data {
    fn fmt(&self, f: &mut fmt::Formatter) -> std::fmt::Result {
        for row in 0..9 {
            for col in 0..9 {
                let index = Coord::from_parts_for_set(row, col, &Set::Rows).to_vector_index();

                write!(f, "{}", self.solution[index])?;

                if col % 3 == 2 {
                    write!(f, " ").unwrap();
                }
            }

            writeln!(f).unwrap();

            if row % 3 == 2 && row != 8 {
                writeln!(f).unwrap();
            }
        }

        write!(f, "")
    }
}

impl fmt::Debug for Data {
    fn fmt(&self, f: &mut fmt::Formatter) -> std::fmt::Result {
        writeln!(f, "Options")?;
        for row in 0..9 {
            for col in 0..9 {
                for digit in 0..9 {
                    if self.options[row][col].contains(&digit) {
                        write!(f, "{}", digit)?
                    } else {
                        write!(f, " ")?
                    }
                }
                write!(f, " | ")?
            }
            writeln!(f)?
        }

        writeln!(f, "Rows:")?;
        for digit in 0..9 {
            write!(f, "{}: ", digit)?;

            for row in 0..9 {
                for col in 0..9 {
                    if self.digits[digit].rows[row].contains(&col) {
                        write!(f, "{}", col)?
                    } else {
                        write!(f, " ")?
                    }
                }
                write!(f, " | ")?
            }
            writeln!(f)?
        }

        writeln!(f, "Columns:")?;
        for digit in 0..9 {
            write!(f, "{}: ", digit)?;

            for col in 0..9 {
                for row in 0..9 {
                    if self.digits[digit].columns[col].contains(&row) {
                        write!(f, "{}", row)?
                    } else {
                        write!(f, " ")?
                    }
                }
                write!(f, " | ")?
            }
            writeln!(f)?
        }

        writeln!(f, "Blocks:")?;
        for digit in 0..9 {
            write!(f, "{}: ", digit)?;

            for block in 0..9 {
                for index in 0..9 {
                    if self.digits[digit].blocks[block].contains(&index) {
                        write!(f, "{}", index)?
                    } else {
                        write!(f, " ")?
                    }
                }
                write!(f, " | ")?
            }
            writeln!(f)?
        }

        writeln!(f, "Fields:").unwrap();
        writeln!(f, "{}", self)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn push_move_should_pass() {
        let mut moves: VecDeque<Move> = VecDeque::new();
        let mut hist: HashSet<(u32, u32)> = HashSet::new();

        push_move(
            &mut moves,
            Coord::from_parts_for_set(1, 2, &Set::Rows),
            3,
            &mut hist,
        );
        push_move(
            &mut moves,
            Coord::from_parts_for_set(3, 2, &Set::Columns),
            4,
            &mut hist,
        );
        push_move(
            &mut moves,
            Coord::from_parts_for_set(2, 0, &Set::Blocks),
            1,
            &mut hist,
        );

        assert_eq!(
            moves.pop_front().unwrap(),
            Move {
                row: 1,
                col: 2,
                digit: 3
            }
        );
        assert_eq!(
            moves.pop_front().unwrap(),
            Move {
                row: 2,
                col: 3,
                digit: 4
            }
        );
        assert_eq!(
            moves.pop_front().unwrap(),
            Move {
                row: 0,
                col: 6,
                digit: 1
            }
        );
    }

}
