use std::fmt;

#[derive(Clone, Debug)]
pub struct Field {
    locked: bool,
    value: u32,
    row: u32,
    col: u32,
}

impl Field {
    pub fn new(row: u32, col: u32) -> Field {
        Field {
            locked: false,
            value: 10,
            row,
            col,
        }
    }

    pub fn set(&mut self, val: u32) {
        if self.locked && self.value != val {
            panic!(
                "Field already locked in a different value. New: {}, old: {}",
                val, self.value
            );
        }

        if val < 9 {
            self.value = val;

            self.locked = true;
        } else {
            panic!("Attempting to set illegal field value {}", val);
        }
    }

    pub fn get(&self) -> Option<u32> {
        if self.locked {
            Some(self.value)
        } else {
            None
        }
    }

    pub fn get_rowcol(&self) -> (u32, u32) {
        (self.row, self.col)
    }
}

impl fmt::Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.locked {
            write!(f, "{}", self.value + 1)
        } else {
            write!(f, ".")
        }
    }
}

//
// TESTS
//
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic]
    fn field_value_set_too_high() {
        let mut val = Field::new(1, 1);
        val.set(9);
    }

    #[test]
    fn field_value_set_correct() {
        let mut val = Field::new(1, 1);
        val.set(5);
        let ret = val.get();
        match ret {
            Some(val) => assert_eq!(val, 5),
            None => panic!("Get should have returned an option::some"),
        }
    }

    #[test]
    #[should_panic]
    fn field_value_set_twice() {
        let mut val = Field::new(1, 1);
        val.set(3);
        val.set(7);
    }

    #[test]
    fn field_starts_unlocked_should_pass() {
        let field = Field::new(1, 1);

        let val = field.get();

        match val {
            None => (),
            Some(_) => panic!("A new field cannot be locked!"),
        }
    }

    #[test]
    fn field_set_should_lock_should_pass() {
        let mut field = Field::new(1, 1);
        field.set(3);

        match field.get() {
            Some(v) => assert_eq!(v, 3),
            None => panic!("A set field should return the set value!"),
        }
    }
}
