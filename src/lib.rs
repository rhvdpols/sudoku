use std::collections::VecDeque;
use std::error;
use std::fmt;
use std::io;
use std::thread;

pub mod coord;
mod data;
mod field;
pub mod set;
mod smove;

#[derive(Debug)]
struct UnsolvableError();

impl fmt::Display for UnsolvableError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "This thing is unsolvable!")
    }
}
impl error::Error for UnsolvableError {}

#[derive(Debug, PartialOrd, PartialEq, Eq, Ord)]
pub struct Solution {
    pub fields: Vec<Vec<u32>>,
}

pub struct Sudoku {
    moves: VecDeque<smove::Move>,
    data: data::Data,
    step: bool,
    pub solutions: Vec<data::Data>,
}

impl Sudoku {
    pub fn new(input: &str, step: bool) -> Sudoku {
        let solutions = Vec::new();

        // parse input
        let mut moves: VecDeque<smove::Move> = VecDeque::new();

        parse(input, &mut moves);

        Sudoku {
            moves,
            data: data::Data::new(),
            step,
            solutions,
        }
    }

    pub fn solve(&mut self) -> Result<bool, Box<dyn error::Error>> {
        // input moves
        while !self.moves.is_empty() {
            let mv = self.moves.pop_front().unwrap();
            do_move(&mut self.data, mv, self.step);
        }

        let test = self.data.search();

        if test.is_empty() {
            self.solutions.push(self.data.clone());

            return Ok(true);
        }

        let mut possible_solutions = Vec::new();

        // We are not finished solving!
        possible_solutions.push(self.data.clone());

        while !possible_solutions.is_empty() {
            let poss = possible_solutions.pop().unwrap();

            // validate or continue
            let guesses = poss.search();

            if guesses.is_empty() {
                // this is a solution
                self.solutions.push(poss);

                if self.solutions.len() == 100 {
                    return Ok(true);
                }
            } else {
                let mut guess_threads = Vec::new();
                // this is not yet completely solved
                for guess in guesses {
                    // spawn a thread for each guess
                    let data = poss.clone();

                    guess_threads.push(thread::spawn(move || {
                        let mut t_data = data;
                        do_move(&mut t_data, guess, false);
                        t_data
                    }));
                }

                // gather all guesses
                for gt in guess_threads {
                    if let Ok(data) = gt.join() {
                        possible_solutions.push(data);
                    }
                }
            }
        }

        if self.solutions.is_empty() {
            return Err(Box::new(UnsolvableError()));
        }

        Ok(true)
    }

    pub fn get_solutions(&self) -> Vec<Solution> {
        let mut list = Vec::new();

        for data in self.solutions.iter() {
            let mut matrix: Vec<Vec<u32>> = Vec::new();
            for _ in 0..9 {
                matrix.push(vec![0, 0, 0, 0, 0, 0, 0, 0, 0]);
            }

            for field in data.solution.iter() {
                let (row, col) = field.get_rowcol();
                let digit = field.get().unwrap();
                matrix[row as usize][col as usize] = digit;
            }

            list.push(Solution { fields: matrix });
        }

        list
    }
}

fn do_move(data: &mut data::Data, mv: smove::Move, step: bool) {
    let mut moves = VecDeque::new();
    moves.push_back(mv);

    while !moves.is_empty() {
        let mv = moves.pop_front().unwrap();
        if step {
            println!("{:?}", mv);
        }

        data.set(mv, &mut moves);
        if step {
            println!("{:?}", data);

            let mut inp = String::new();
            let _ = io::stdin().read_line(&mut inp);
        }
    }
}

impl fmt::Display for Sudoku {
    fn fmt(&self, f: &mut fmt::Formatter) -> std::fmt::Result {
        for sol in self.solutions.iter() {
            write!(f, "{}", sol)?;
            writeln!(f, "___________\n")?;
        }
        write!(f, "")
    }
}

impl fmt::Debug for Sudoku {
    fn fmt(&self, f: &mut fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self.data)
    }
}

fn parse(input: &str, moves: &mut VecDeque<smove::Move>) {
    let mut row: u32 = 0;
    let mut col: u32 = 0;

    for line in input.lines() {
        if line.is_empty() {
            continue;
        }

        for c in line.chars() {
            if c.eq_ignore_ascii_case(&' ') {
                continue;
            }

            if c.is_digit(10) {
                let digit = c.to_digit(10).unwrap();

                moves.push_back(smove::Move {
                    col,
                    row,
                    digit: digit - 1,
                });
            }

            col += 1;
        }

        col = 0;
        row += 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_should_pass() {
        let mut moves: VecDeque<smove::Move> = VecDeque::new();
        let sdk = String::from("1x x\nx 2x\n\n xx3");

        parse(&sdk, &mut moves);

        assert_eq!(moves.len(), 3);
        assert_eq!(
            moves.pop_front().unwrap(),
            smove::Move {
                col: 0,
                row: 0,
                digit: 0
            }
        );
        assert_eq!(
            moves.pop_front().unwrap(),
            smove::Move {
                col: 1,
                row: 1,
                digit: 1
            }
        );
        assert_eq!(
            moves.pop_front().unwrap(),
            smove::Move {
                col: 2,
                row: 2,
                digit: 2
            }
        );
    }
}
