use std::env;
use std::fs;
use std::panic;
use std::time::Instant;

extern crate sudoku;

fn main() {
    let args: Vec<String> = env::args().collect();

    let mut step_through = false;
    for arg in args {
        if arg.contains("step") {
            step_through = true;
        }
        if !arg.contains("verbose") {
            panic::set_hook(Box::new(|_info| {}));
        }
    }

    for pos_entry in fs::read_dir("./input").expect("Unable to open directory \"input\".") {
        let entry = pos_entry.expect("An error occured while walking the \"input\" directory");

        if entry.path().is_dir() {
            // skip subdirectories
            continue;
        };

        let sdk = fs::read_to_string(entry.path()).expect("An error occured while reading file");

        let start = Instant::now();

        let mut sudoku = sudoku::Sudoku::new(&sdk, step_through);

        let success = sudoku.solve().unwrap_or_else(|error| {
            println!(
                "Error occured when trying to solve {:?}: {}",
                entry.path(),
                error
            );
            false
        });

        let time_spent = start.elapsed().as_micros();

        if success {
            println!(
                "Success for {:?}, {} solution(s) found ({} microseconds):",
                entry.path(),
                sudoku.solutions.len(),
                time_spent
            );
            println!("{}", sudoku);
        } else {
            println!("Unable to solve {:?} ({} microseconds).", entry.path(), time_spent);
        }
    }
}
