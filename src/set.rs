#[derive(Debug)]
pub enum Set {
    Rows,
    Columns,
    Blocks,
}