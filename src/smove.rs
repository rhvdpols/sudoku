#[derive(Debug, PartialEq)]
pub struct Move {
    pub row: u32,
    pub col: u32,
    pub digit: u32,
}