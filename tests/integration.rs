use std::collections::HashSet;

use sudoku::coord;
use sudoku::set;

#[test]
fn are_solutions_valid() {
    let sdk = String::from("5xx x6x x3x\nx8x xxx x72\nxx3 x2x 9xx\n\nxx1 xx7 xxx\nx9x 3x6 x8x\nxxx 2xx 6xx\n\nxx5 x4x 1xx\n34x xxx x5x\nxx2 x7x xx6");

    let mut sudoku = sudoku::Sudoku::new(&sdk, false);

    assert_eq!(sudoku.solve().unwrap(), true);

    println!("{}", sudoku);

    println!("{:?}", sudoku.get_solutions());

    for solution in sudoku.get_solutions() {
        assert_eq!(true, check_if_valid(solution));
    }
}

#[test]
fn are_solutions_unique() {
    let sdk = String::from("xxx xxx xxx\nxxx xxx xxx\nxxx xxx xxx\nxxx xxx xxx\nxxx xxx xxx\nxxx xxx xxx\nxxx xxx xxx\nxxx xxx xxx\nxxx xxx xxx\n");

    let mut sudoku = sudoku::Sudoku::new(&sdk, false);

    assert_eq!(sudoku.solve().unwrap(), true);

    let mut solutions = sudoku.get_solutions();

    solutions.sort_unstable();

    let num = solutions.len();

    println!("{}", num);

    solutions.dedup_by(|a, b| a == b);

    assert_eq!(num, solutions.len());
}

#[test]
#[should_panic]
fn invalid_sudoku_should_panic() {
    let sdk = String::from("5xx x6x x3x\nx8x xxx x72\nxx3 x2x 9xx\n\nxx1 xx7 xxx\nx9x 3x6 x8x\nxxx 2xx 6xx\n\nxx5 x5x 1xx\n34x xxx x5x\nxx2 x7x xx6");

    let mut sudoku = sudoku::Sudoku::new(&sdk, false);

    sudoku.solve().unwrap();
}

fn check_if_valid(solution: sudoku::Solution) -> bool {
    for i in 0..9 {
        let mut rowdigits: HashSet<u32> = (0..9).collect();
        let mut columndigits: HashSet<u32> = (0..9).collect();
        let mut blockdigits: HashSet<u32> = (0..9).collect();

        for j in 0..9 {
            let (block, index) = coord::Coord::from_parts_for_set(i, j, &set::Set::Rows)
                .to_parts_for_set(&set::Set::Blocks);

            if !rowdigits.remove(&solution.fields[i as usize][j as usize])
                || !columndigits.remove(&solution.fields[j as usize][i as usize])
                || !blockdigits.remove(&solution.fields[block as usize][index as usize])
            {
                return false;
            }
        }
    }

    true
}
